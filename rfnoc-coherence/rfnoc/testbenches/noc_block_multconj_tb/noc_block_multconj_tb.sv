/*
 * Copyright 2020 <+YOU OR YOUR COMPANY+>.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

`timescale 1ns/1ps
`define NS_PER_TICK 1
`define NUM_TEST_CASES 4

`include "sim_exec_report.vh"
`include "sim_clks_rsts.vh"
`include "sim_rfnoc_lib.svh"

module noc_block_multconj_tb();
   `TEST_BENCH_INIT("noc_block_multconj",`NUM_TEST_CASES,`NS_PER_TICK);
   localparam BUS_CLK_PERIOD = $ceil(1e9/166.67e6);
   localparam CE_CLK_PERIOD  = $ceil(1e9/200e6);
   localparam NUM_CE         = 1;  // Number of Computation Engines / User RFNoC blocks to simulate
   localparam NUM_STREAMS    = 2;  // Number of test bench streams
   `RFNOC_SIM_INIT(NUM_CE, NUM_STREAMS, BUS_CLK_PERIOD, CE_CLK_PERIOD);
   `RFNOC_ADD_BLOCK(noc_block_multconj, 0);

   localparam SPP = 16; // Samples per packet

   /********************************************************
    ** Verification
    ********************************************************/
   initial begin : tb_main
      string s;
      logic [31:0] random_word;
      logic [63:0] readback;

      /********************************************************
       ** Test 1 -- Reset
       ********************************************************/
      `TEST_CASE_START("Wait for Reset");
      while (bus_rst) @(posedge bus_clk);
      while (ce_rst) @(posedge ce_clk);
      `TEST_CASE_DONE(~bus_rst & ~ce_rst);

      /********************************************************
       ** Test 2 -- Check for correct NoC IDs
       ********************************************************/
      `TEST_CASE_START("Check NoC ID");
      // Read NOC IDs
      tb_streamer.read_reg(
                           sid_noc_block_multconj,
                           RB_NOC_ID,
                           readback);
      $display("Read multconj NOC ID: %16x", readback);
      `ASSERT_ERROR(
                    readback == noc_block_multconj.NOC_ID,
                    "Incorrect NOC ID");
      `TEST_CASE_DONE(1);

      /********************************************************
       ** Test 3 -- Connect RFNoC blocks
       ********************************************************/
      `TEST_CASE_START("Connect RFNoC blocks");
      `RFNOC_CONNECT_BLOCK_PORT(
                                noc_block_tb,
                                0,
                                noc_block_multconj,
                                0,
                                SC16,
                                SPP);
      `RFNOC_CONNECT_BLOCK_PORT(
                                noc_block_tb,
                                1,
                                noc_block_multconj,
                                1,
                                SC16,
                                SPP);
      `RFNOC_CONNECT(noc_block_multconj,noc_block_tb,SC16,SPP);
      `TEST_CASE_DONE(1);

      /********************************************************
       ** Test 4  - Test multiply conjugate
       ********************************************************/
      `TEST_CASE_START("Test multiply conjugate ramps");
      fork
         begin
            cvita_payload_t send_payload_0, send_payload_1;
            cvita_metadata_t tx_md;
            for (int i = 0; i < SPP/2; i++) begin
                automatic logic [15:0] a0 = 16'(4*i);
                automatic logic [15:0] b0 = 16'(4*i+1);
                automatic logic [15:0] a1 = 16'(4*i+SPP);
                automatic logic [15:0] b1 = 16'(4*i+1+SPP);
                automatic logic [15:0] a2 = 16'(4*i+2);
                automatic logic [15:0] b2 = 16'(4*i+3);
                automatic logic [15:0] a3 = 16'(4*i+2+SPP);
                automatic logic [15:0] b3 = 16'(4*i+3+SPP);
                send_payload_0.push_back({a0,b0,a1,b1});
                send_payload_1.push_back({a2,b2,a3,b3});
            end
            tx_md.eob = 1'b1;
            tb_streamer.send(send_payload_0,tx_md,0);
            tb_streamer.send(send_payload_1,tx_md,1);
         end
         begin
            cvita_payload_t recv_payload;
            cvita_metadata_t rx_md;
            logic [63:0] expected_value, recv_value;
            tb_streamer.recv(recv_payload,rx_md,0);
            for (int i = 0; i < SPP/2; i++) begin
                automatic logic [15:0] a0_ = 16'(4*i);
                automatic logic [15:0] b0_ = 16'(4*i+1);
                automatic logic [15:0] a1_ = 16'(4*i+SPP);
                automatic logic [15:0] b1_ = 16'(4*i+1+SPP);
                automatic logic [15:0] a2_ = 16'(4*i+2);
                automatic logic [15:0] b2_ = 16'(4*i+3);
                automatic logic [15:0] a3_ = 16'(4*i+2+SPP);
                automatic logic [15:0] b3_ = 16'(4*i+3+SPP);
                expected_value = { a0_*a2_+b0_*b2_, b0_*a2_-a0_*b2_, a1_*a3_+b1_*b3_, b1_*a3_-a1_*b3_ };
                recv_value = recv_payload[i];
                $sformat(s, "Incorrect value received! Expected: %0d+j%0d, %0d+j%0d Received: %0d+j%0d, %0d+j%0d", expected_value[63:48], expected_value[47:33], expected_value[32:16], expected_value[15:0], recv_value[63:48], recv_value[47:33], recv_value[32:16], recv_value[15:0]);
                `ASSERT_ERROR(recv_payload[i] == expected_value, s);
            end
         end
      join
      `TEST_CASE_DONE(1);
      `TEST_BENCH_DONE;

   end
endmodule
