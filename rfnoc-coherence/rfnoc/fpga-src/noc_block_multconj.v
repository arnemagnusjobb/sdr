//
/*
 * Copyright 2020 <+YOU OR YOUR COMPANY+>.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

module noc_block_multconj #(
                            parameter NOC_ID = 64'hFF10000000000001,
                            parameter STR_SINK_FIFOSIZE = 11)
   (
    input         bus_clk, input bus_rst,
    input         ce_clk, input ce_rst,
    input [63:0]  i_tdata, input i_tlast, input i_tvalid, output i_tready,
    output [63:0] o_tdata, output o_tlast, output o_tvalid, input o_tready,
    output [63:0] debug
    );

   wire [63:0] cmdout_tdata, ackin_tdata;
   wire        cmdout_tlast, cmdout_tvalid, cmdout_tready, ackin_tlast, ackin_tvalid, ackin_tready;
   // Control Source Unused
   assign cmdout_tdata  = 64'd0;
   assign cmdout_tlast  = 1'b0;
   assign cmdout_tvalid = 1'b0;
   assign ackin_tready  = 1'b1;

   ////////////////////////////////////////////////////////////
   //
   // Arne: Two inputs
   //
   ////////////////////////////////////////////////////////////

   wire [63:0]    str_sink_tdata[0:1];
   wire [1:0]     str_sink_tlast, str_sink_tvalid, str_sink_tready;

   ////////////////////////////////////////////////////////////
   //
   // Arne: Single output
   //
   ////////////////////////////////////////////////////////////

   wire [63:0]    str_src_tdata;
   wire           str_src_tlast, str_src_tvalid, str_src_tready;

   wire [1:0]     clear_tx_seqnum, clear_tx_seqnum_bclk;
   wire [15:0]    src_sid[0:1], next_dst_sid[0:1], resp_in_dst_sid[0:1], resp_out_dst_sid;

   ////////////////////////////////////////////////////////////
   //
   // Arne: Clock synchronization / crossing clock domains
   //
   ////////////////////////////////////////////////////////////

   synchronizer #(
                  .INITIAL_VAL(1'b0),
                  .WIDTH(2))
   clear_tx_sync_i (
                    .clk(bus_clk),
                    .rst(1'b0),
                    .in(clear_tx_seqnum),
                    .out(clear_tx_seqnum_bclk));

   ////////////////////////////////////////////////////////////
   //
   // RFNoC Shell
   //
   ////////////////////////////////////////////////////////////

   noc_shell #(
               .NOC_ID(NOC_ID),
               .STR_SINK_FIFOSIZE({2{STR_SINK_FIFOSIZE[7:0]}}),
               .INPUT_PORTS(2),
               .OUTPUT_PORTS(1))
   noc_shell (
              .bus_clk(bus_clk), .bus_rst(bus_rst),
              .i_tdata(i_tdata), .i_tlast(i_tlast), .i_tvalid(i_tvalid), .i_tready(i_tready),
              .o_tdata(o_tdata), .o_tlast(o_tlast), .o_tvalid(o_tvalid), .o_tready(o_tready),
              // Compute Engine Clock Domain
              .clk(ce_clk), .reset(ce_rst),
              // Control Sink
              .set_data(), .set_addr(), .set_stb(), .set_time(), .set_has_time(),
              .rb_stb(2'b11), .rb_data(128'd0), .rb_addr(),
              // Control Source Unused
              .cmdout_tdata(cmdout_tdata), .cmdout_tlast(cmdout_tlast), .cmdout_tvalid(cmdout_tvalid), .cmdout_tready(cmdout_tready),
              .ackin_tdata(ackin_tdata), .ackin_tlast(ackin_tlast), .ackin_tvalid(ackin_tvalid), .ackin_tready(ackin_tready),
              // Stream Sink
              .str_sink_tdata({str_sink_tdata[1], str_sink_tdata[0]}), .str_sink_tlast(str_sink_tlast), .str_sink_tvalid(str_sink_tvalid), .str_sink_tready(str_sink_tready),
              // Stream Source
              .str_src_tdata(str_src_tdata), .str_src_tlast(str_src_tlast), .str_src_tvalid(str_src_tvalid), .str_src_tready(str_src_tready),
              // Misc
              .vita_time(), .clear_tx_seqnum(clear_tx_seqnum),
              .src_sid({src_sid[1],src_sid[0]}), .next_dst_sid({next_dst_sid[1], next_dst_sid[0]}),
              .resp_in_dst_sid({resp_in_dst_sid[1],resp_in_dst_sid[0]}), .resp_out_dst_sid(resp_out_dst_sid),
              .debug(debug));

   ////////////////////////////////////////////////////////////
   //
   // AXI Wrapper
   // Convert RFNoC Shell interface into AXI stream interface
   //
   ////////////////////////////////////////////////////////////
   wire [31:0] m_axis_data_tdata[0:1];
   wire [1:0]  m_axis_data_tlast;
   wire [1:0]  m_axis_data_tvalid;
   wire [1:0]  m_axis_data_tready;
   wire [127:0] m_axis_data_tuser[0:1];

   wire [31:0] s_axis_data_tdata;
   wire        s_axis_data_tlast;
   wire        s_axis_data_tvalid;
   wire        s_axis_data_tready;
   wire [127:0] s_axis_data_tuser;

   ///////////////////////////////////////////////////////////
   //
   // Two inputs, one output.
   // Need to use MAX(NUM_INPUTS, NUM_OUTPUTS) AXI Wrappers.
   // If there is a disparity, for one of the AXI Wrappers,
   // only the input or output (as needed) are used.
   //
   // Here, we use two AXI Wrappers:
   //    axi_wrapper_0: input 0, output 0
   //    axi_wrapper_1: input 1
   //
   ///////////////////////////////////////////////////////////


   axi_wrapper #(
       .SIMPLE_MODE(1) /* Handle header internally*/ )
   axi_wrapper_0 (
       .clk(ce_clk), .reset(ce_rst),
       .bus_clk(bus_clk), .bus_rst(bus_rst),
       .clear_tx_seqnum(clear_tx_seqnum[0]),
       .next_dst(next_dst_sid[0]),
       .set_stb(), .set_addr(), .set_data(),
       .i_tdata(str_sink_tdata[0]), .i_tlast(str_sink_tlast[0]), .i_tvalid(str_sink_tvalid[0]), .i_tready(str_sink_tready[0]),
       .o_tdata(str_src_tdata), .o_tlast(str_src_tlast), .o_tvalid(str_src_tvalid), .o_tready(str_src_tready),
       .m_axis_data_tdata(m_axis_data_tdata[0]),
       .m_axis_data_tlast(m_axis_data_tlast[0]),
       .m_axis_data_tvalid(m_axis_data_tvalid[0]),
       .m_axis_data_tready(m_axis_data_tready[0]),
       .m_axis_data_tuser(m_axis_data_tuser[0]),
       .s_axis_data_tdata(s_axis_data_tdata),
       .s_axis_data_tlast(s_axis_data_tlast),
       .s_axis_data_tvalid(s_axis_data_tvalid),
       .s_axis_data_tready(s_axis_data_tready),
       .s_axis_data_tuser(s_axis_data_tuser),
       .m_axis_config_tdata(),
       .m_axis_config_tlast(),
       .m_axis_config_tvalid(),
       .m_axis_config_tready(),
       .m_axis_pkt_len_tdata(),
       .m_axis_pkt_len_tvalid(),
       .m_axis_pkt_len_tready()
   );

   axi_wrapper #(
       .SIMPLE_MODE(0) /* Handle header internally*/ )
   axi_wrapper_1 (
       .clk(ce_clk), .reset(ce_rst),
       .bus_clk(bus_clk), .bus_rst(bus_rst),
       .clear_tx_seqnum(clear_tx_seqnum[1]),
       .next_dst(next_dst_sid[1]),
       .set_stb(), .set_addr(), .set_data(),
       // Only using output to handle the header. Input data (s_axis_data_tdata) is unused.
       .i_tdata(str_sink_tdata[1]), .i_tlast(str_sink_tlast[1]), .i_tvalid(str_sink_tvalid[1]), .i_tready(str_sink_tready[1]),
       .o_tdata(), .o_tlast(), .o_tvalid(), .o_tready(),
       .m_axis_data_tdata(m_axis_data_tdata[1]),
       .m_axis_data_tlast(m_axis_data_tlast[1]),
       .m_axis_data_tvalid(m_axis_data_tvalid[1]),
       .m_axis_data_tready(m_axis_data_tready[1]),
       .m_axis_data_tuser(m_axis_data_tuser[1]),
       .s_axis_data_tdata(),
       .s_axis_data_tlast(),
       .s_axis_data_tvalid(),
       .s_axis_data_tready(),
       .s_axis_data_tuser(),
       .m_axis_config_tdata(),
       .m_axis_config_tlast(),
       .m_axis_config_tvalid(),
       .m_axis_config_tready(),
       .m_axis_pkt_len_tdata(),
       .m_axis_pkt_len_tvalid(),
       .m_axis_pkt_len_tready()
   );


   ///////////////////////////////////////////////////////////
   //
   // ORing ce_rst with clear_tx_seqnum_bclk[0] as the latter
   // is strobed by UHD when the block is enumerated at
   // startup.
   //
   // Net effect: The block is reset whenever an UHD app or a
   // GNU Radio flowgraph is run.
   //
   // Cf. feedback from JPendlum, April 2nd 2020.
   //
   ///////////////////////////////////////////////////////////


   multconj inst_multconj(
           .clk(ce_clk),
           .reset(ce_rst | clear_tx_seqnum_bclk[0]),
           .i0_tdata(m_axis_data_tdata[0]), .i0_tlast(m_axis_data_tlast[0]), .i0_tvalid(m_axis_data_tvalid[0]), .i0_tready(m_axis_data_tready[0]),
           .i1_tdata(m_axis_data_tdata[1]), .i1_tlast(m_axis_data_tlast[1]), .i1_tvalid(m_axis_data_tvalid[1]), .i1_tready(m_axis_data_tready[1]),
       .prod_tdata(s_axis_data_tdata), .prod_tlast(s_axis_data_tlast), .prod_tvalid(s_axis_data_tvalid), .prod_tready(s_axis_data_tready)
   );

endmodule
