//
// Copyright 2020 Forsvarets Forskningsinstitutt
//
// Multiply input 0 with the complex conjugate of input 1.
//

module multconj
   (input clk, input reset,
    input [31:0] i0_tdata, input i0_tlast, input i0_tvalid, output i0_tready,
    input [31:0] i1_tdata, input i1_tlast, input i1_tvalid, output i1_tready,
    output [31:0] prod_tdata, output prod_tlast, output prod_tvalid, input prod_tready);

   ///////////////////////////////////////////////////////////
   //
   // Adding wires and FIFOs to ensure Pipeline (cf. gain
   // module)
   //
   ///////////////////////////////////////////////////////////

   wire [31:0] int_fac0_tdata;
   wire        int_fac0_tlast, int_fac0_tvalid, int_fac0_tready;

   wire [31:0] int_fac1_tdata;
   wire        int_fac1_tlast, int_fac1_tvalid, int_fac1_tready;

   wire [31:0] int_prod_tdata;
   wire        int_prod_tlast, int_prod_tvalid, int_prod_tready;

   axi_fifo_flop #(.WIDTH(32+1))
   pipeline_fac0_axi_fifo_flop(
       .clk(clk), .reset(reset), .clear(),
       .i_tdata({i0_tlast, i0_tdata}),
       .i_tvalid(i0_tvalid),
       .i_tready(i0_tready),
       .o_tdata({int_fac0_tlast, int_fac0_tdata}),
       .o_tvalid(int_fac0_tvalid),
       .o_tready(int_fac0_tready)
   );

   axi_fifo_flop #(.WIDTH(32+1))
   pipeline_fac1_axi_fifo_flop(
       .clk(clk), .reset(reset), .clear(),
       .i_tdata({i1_tlast, i1_tdata}),
       .i_tvalid(i1_tvalid),
       .i_tready(i1_tready),
       .o_tdata({int_fac1_tlast, int_fac1_tdata}),
       .o_tvalid(int_fac1_tvalid),
       .o_tready(int_fac1_tready)
   );

   axi_fifo_flop #(
       .WIDTH(32+1)
   ) pipeline_prod_axi_fifo_flop(
       .clk(clk), .reset(reset), .clear(),
       .i_tdata({int_prod_tlast, int_prod_tdata}),
       .i_tvalid(int_prod_tvalid),
       .i_tready(int_prod_tready),
       .o_tdata({prod_tlast, prod_tdata}),
       .o_tvalid(prod_tvalid),
       .o_tready(prod_tready)
   );

   /////////////////////////////////////////////////////////////
   //
   // Flow control logic
   //
   /////////////////////////////////////////////////////////////


   assign int_prod_tvalid = int_fac0_tvalid & int_fac1_tvalid;
   assign int_fac0_tready = int_prod_tvalid & int_prod_tready;
   assign int_fac1_tready = int_prod_tvalid & int_prod_tready;

   // Following zeroeth input
   assign int_prod_tlast = int_fac0_tlast;

   /////////////////////////////////////////////////////////////
   //
   // Let z0 = a0 + jb0
   //     z1 = a1 + jb1
   //
   // Then:
   //     z0 x z1* = (a0 + jb0) x (a1 - jb1)
   //              = a0 x a1 + b0 x b1 + j x (b0 x a1 - a0 x b1)
   //
   //
   // Allocating twice as large registers (in no. of bits)
   // as the input widths for each multiplication in the above,
   // in order to combat overflow errors.
   //
   /////////////////////////////////////////////////////////////

   wire [15:0] a0 = int_fac0_tdata[31:16];
   wire [15:0] b0 = int_fac0_tdata[15:0];

   wire [15:0] a1 = int_fac1_tdata[31:16];
   wire [15:0] b1 = int_fac1_tdata[15:0];

   wire [31:0] re_prod_0 = a0 * a1;
   wire [31:0] re_prod_1 = b0 * b1;

   wire [31:0] im_prod_0 = b0 * a1;
   wire [31:0] im_prod_1 = a0 * b1;

   /////////////////////////////////////////////////////////////
   //
   // Again allocating twice as large registers (in no. of bits)
   // as the input widths for the real and imaginary parts, in
   // order to combat overflow errors.
   //
   /////////////////////////////////////////////////////////////

   wire [31:0] re = re_prod_0 + re_prod_1;
   wire [31:0] im = im_prod_0 - im_prod_1;

   /////////////////////////////////////////////////////////////
   //
   // Truncating the output data
   //
   /////////////////////////////////////////////////////////////

   assign int_prod_tdata = { re[15:0], im[15:0] };



endmodule // multconj

