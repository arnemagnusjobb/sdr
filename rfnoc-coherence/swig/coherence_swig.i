/* -*- c++ -*- */

#define COHERENCE_API
#define ETTUS_API

%include "gnuradio.i"/*			*/// the common stuff

//load generated python docstrings
%include "coherence_swig_doc.i"
//Header from gr-ettus
%include "ettus/device3.h"
%include "ettus/rfnoc_block.h"
%include "ettus/rfnoc_block_impl.h"

%{
#include "ettus/device3.h"
#include "ettus/rfnoc_block_impl.h"
#include "coherence/multconj.h"
%}

%include "coherence/multconj.h"
GR_SWIG_BLOCK_MAGIC2(coherence, multconj);
