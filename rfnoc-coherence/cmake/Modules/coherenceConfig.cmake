INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_COHERENCE coherence)

FIND_PATH(
    COHERENCE_INCLUDE_DIRS
    NAMES coherence/api.h
    HINTS $ENV{COHERENCE_DIR}/include
        ${PC_COHERENCE_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    COHERENCE_LIBRARIES
    NAMES gnuradio-coherence
    HINTS $ENV{COHERENCE_DIR}/lib
        ${PC_COHERENCE_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(COHERENCE DEFAULT_MSG COHERENCE_LIBRARIES COHERENCE_INCLUDE_DIRS)
MARK_AS_ADVANCED(COHERENCE_LIBRARIES COHERENCE_INCLUDE_DIRS)

