Her putter vi scripts vi kan få bruk for, etter installasjon av UHD, GNU Radio og gr-ettus.

I første rekke forutsettes at miljøvariabelen $PREFIX peker til mappa som inneholder kildekode- og installasjonsfilene.

Viktig at $PATH inkluderer mappa hvor dette skriptet legges, eventuelt at brukeren definerer et passende alias.
